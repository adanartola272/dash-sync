<?php
/**
 * @author Whyte Enterprises
 * @package  DASH-Sync 
 */

/**
 * Receptor class
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

class MyCron {

    protected $logger;

    public function __construct() {
        $this->load_libraries();
        add_filter('cron_schedules', array($this, 'cron_time_intervals'));
        add_action( 'wp',  array($this, 'cron_scheduler'));
        add_action( 'synchronize_data', array( $this, 'synchronize' ) );
       // $this->synchronize();
     }

    public function cron_time_intervals( $schedules )
    {
        $schedules['10sec'] = array(
            'interval' => 10, //10 * 60,
            'display' => 'Once_10',
        );
        return $schedules;
    }

    public function cron_scheduler() 
    {
        if ( ! wp_next_scheduled( 'synchronize_data' ) ) {
            wp_schedule_event( time(), '10sec', 'synchronize_data');
        }
    }

    private function load_libraries()
    {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'libraries/main.php';
        $this->DASHSync  = new DASHSync();

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'libraries/logger.php';
        $this->logger  = new Logger();

    }

    public function synchronize()
    {       

        if ( !$this->logger->log_exists() ) 
        {
            $this->logger->create_file_log();
            
            $this->DASHSync->run();

        } else {

            $response = $this->logger->get_last_event();             
                
            $data = $response['data'];                

            if ( defined("START_TIME") ){
                
                $last_date = date( "Y-m-d", strtotime( $data['date'] ) ) . START_TIME;
            
            }else{

                $last_date = date( "Y-m-d", strtotime( $data['date'] ) ) . "22:00:00";

            }

            $current_date = date("Y-m-d H:i:s");
            $date_diff = $this->logger->time_diff( $current_date, $last_date  );

            if ( $this->logger->is_time_range( $current_date ) ){
                    
                if( $date_diff >= 1 ){

                    $this->DASHSync->run();

                }elseif( $data['event']['error'] ){
                            
                    $this->DASHSync->run();
                    
                }   

            }         
        }
    }  
    
}