<?php

/**
 * @author Whyte Enterprises
 * @package  DASH-Sync 
 */

/**
 * DASH_Sync deactivation class
 */
class Deactivate_dash_sync{
    /**
     * Activation procedure
     */
    public static function deactivate()

    
    {
        require_once plugin_dir_path( __FILE__ ) . '/db_manager.php';

        $db_manager = new DBManager;
        $db_manager->truncate_facility_table();
        $db_manager->truncate_eventType_table();
        $db_manager->truncate_resource_table();
        $db_manager->truncate_event_table();
        $db_manager->truncate_season_table();
        $db_manager->truncate_league_table();
        $db_manager->truncate_team_table();
        $db_manager->truncate_eventType_category_table();
        #$db_manager->delete_postmeta_rows_query();
        #$db_manager->delete_posts_rows_query();
    }
}