<?php

/**
 * @author Whyte Enterprises
 * @package  DASH-Sync 
 */

/**
 * Receptor class
 */
class Logger {

    protected $log_file;
    protected $file_manager; 



    public function __construct(){
        
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'libraries/file_manager.php';
        
        $this->file_manager = new FileManager(); 
        
        $this->log_file = plugin_dir_path( dirname( __FILE__ ) ) . 'assets/config_files/log.csv';
    }

    public function log_exists(){
        return is_file( $this->log_file );
    }

    public function create_file_log(){
        $event = array( 'error' => false, 
                        'data'  => '',
                        'exception'  => '', 
                        'message' => 'Log created.', 
                    );

        $content = array ( date("Y-m-d H:i:s"), serialize( $event )  );

        $content = implode(",",$content)."\n";

        $this->file_manager->create_file( $this->log_file, $content );
    }

    public function write_to_Log( $event )
    {
        if ( isset($event['exception']) ){
            $event['exception'] = str_replace(","," ",$event['exception']);
        }

        $content = array ( date("Y-m-d H:i:s"), serialize( $event )  );

        $content = implode(",",$content) . "\n";

        if (is_file( $this->log_file )){

            $this->file_manager::writefile( $this->log_file, $content );
        
        }else {
            var_dump("Config file: 'assets/config_files/log.csv' or folder 'assets/config_files' ".
                     " not found, please create before continue!!!");
            die;    
        }

    }

    public function get_last_event(){
        return $this->read_from_log( $this->log_file );
    }

    private function read_from_log( $filename )
    {
       $response = $this->file_manager::read_csv_file( $filename );
       
       if ( !$response["error"] ){
            
            $last_event = end( $response["data"] );
            
            $date =  $last_event[0];
            $event = unserialize ( $last_event[1] );

            return array(
               'error' => false,
               'data' => array(
                'date' => $date,
                'event'=> $event,
               ),               
            );

      } else{
        return array(
            'error' => true,
            'data' => '',
            'Message' => 'Error log not found!',               
         );
      }

    }  

    public function time_diff( $date1, $date2 )
    {
        $date1=strtotime( $date1 );
        $date2=strtotime( $date2 );
        return ( $date1 - $date2 ) /60/60/24;
    }

    public function is_time_range( $current_date )
    {      
      
        if ( defined( "START_TIME" ) and defined( "RANGE_OF_TIME" ) ){           

            if ( is_numeric(RANGE_OF_TIME) and $this->is_time( START_TIME ) ){ 
                
                $hour_range = abs (( int )RANGE_OF_TIME);
                
                $start_date  = strtotime( date("Y-m-d ") . START_TIME );
                $end_date = strtotime(sprintf("+%d hours", $hour_range),$start_date);
                $current_date = strtotime($current_date);                
                return ($current_date >= $start_date) && ($current_date <= $end_date);
            }
        }

        
        $start_date = date("Y-m-d")." 22:00:00";
        $end_date = date( "Y-m-d H:i:s" , strtotime( "+5 hours", strtotime( $start_date ) ) );

       return $this->check_in_range($start_date, $end_date, $current_date);
    }

    public function is_time( $time ){
        $result = preg_match('#^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$#',$time);
        return $result;
    }

    public function check_in_range($start_date, $end_date, $date_from_user)
    {
        // Convert to timestamp
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $user_ts = strtotime($date_from_user);

        // Check that user date is between start & end
        return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
    }

}
