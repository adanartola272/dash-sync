<?php

/**
 * @author Whyte Enterprises
 * @package  DASH-Sync 
 */

/**
 * Receptor class
 */
class DBManager{

    /** 
     * Parameters of the facility table.
     * @var array 
     * @access protected 
     */
    protected $facility_parameters;
    
    /** 
     * Parameters of the resource table.
     * @var array 
     * @access protected 
     */
    protected $resource_parameters;

    /** 
     * Parameters of the eventType table.
     * @var array 
     * @access protected 
     */
    protected $eventType_parameters;

    /** 
     * Parameters of the event table.
     * @var array 
     * @access protected 
     */
    protected $event_parameters;
     
    /** 
     *  Parameters of the season table.
     * @var array 
     * @access protected 
     */
    protected $season_parameters;

     /** 
     *  Parameters of the league table.
     * @var array 
     * @access protected 
     */
    protected $league_parameters;

     /** 
     *  Parameters of the team table.
     * @var array 
     * @access protected 
     */
    protected $team_parameters;
    
    /** 
     * Parameters of the mec_events table.
     * @var array 
     * @access protected 
     */
    protected $mec_events_parameters;

    /** 
     * Parameters of the mec_dates table.
     * @var array 
     * @access protected 
     */
    protected $mec_dates_parameters;
    
    /** 
     * Parameters of the term_relationships table.
     * @var array 
     * @access protected 
     */
    protected $term_relationships_parameters;

    /** 
     * Parameters of the  eventType, mec_category relationship table.
     * @var array 
     * @access protected 
     */
    protected $eventType_category_parameters;

    /** 
     * Prefix of wordpress database.
     * @var string
     * @access protected 
     */
    protected $wpPrefix;

    /** 
     * Plugin's database prefix.
     * @var string 
     * @access protected 
     */
    protected $prefix;

    

    public function __construct()
    {
       $this->init_tables_parameters();
       global $wpdb;  
       $this->wpPrefix = $wpdb->prefix;

       if ( defined( DASH_SYNC_DB_PREFIX ) ){
            $this->prefix = DASH_SYNC_DB_PREFIX;
       }else{
            $this->prefix = "das_";
       }
       
    }

    /**
     * Init each tab13le paramaters names.
     */
    public function init_tables_parameters()
    {
        $this->facility_parameters = array(
            "location",
            "facilityId",
            "mecLocationId",
        );

        $this->resource_parameters = array(
            "resourceId",
            "name",
            "facilityId",
        );

        $this->eventType_parameters = array(
            "eventTypeId",
            "code",
            "name",
            "hexColour",
            "apiCall",
        );

        $this->event_parameters = array(
            "eventId",
            "hteamId",
            "start",
            "end",
            "description",
            "eventTypeCode",
            "resourceId",
        );

        $this->season_parameters = array(
            "seasonId",
            "name",
            "start",
            "end",
            "facilityId",
        );

        $this->league_parameters = array(
            "leagueId",
            "name",
            "info",
            "start",
            "seasonId",
            "facilityId",
        );

        $this->team_parameters = array(
            "teamId",
            "name",
            "start",
            "leagueId",
            "SeasonId",
            "facilityId",
        );

        $this->mec_events_parameters = array(
            "id", "post_id", "start", "end", "repeat",
            "rinterval", "year", "month", "day", "week",
            "weekday","weekdays", "days", "not_in_days",
            "time_start", "time_end",
        );

        $this->mec_dates_parameters = array(
            "id", 
            "post_id", 
            "dstart", 
            "dend", 
            "tstart",
            "tend",
        );

        $this->term_relationships_parameters = array(
            "object_id",
            "term_taxonomy_id",
            "term_order",
        );

        $this->eventType_category_parameters = array(
            "eventTypeCode",
            "mec_category_taxonomy_term_id",
        );
    }

    /**
     * Insert data in facility table .
     * @param array $values Values to insert into the facility table.
     * @return int|bool return $wpdb result  
     */
    public function insert_facility( $values )
    {
        $table_name = $this->wpPrefix . $this->prefix . "facility";
        return $this->insert_query( $table_name , $this->facility_parameters, $values );
    }

    /**
     * Insert data into resource table.
     * @param array $values Values to insert into the resource table.
     * @return int|bool return $wpdb result  
     */
    public function insert_resource( $values )
    {
        $table_name = $this->wpPrefix . $this->prefix . "resource";
        return $this->insert_query( $table_name, $this->resource_parameters, $values );
    }

    /**
     * Insert data into eventType table.
     * @param array $values Values to insert into the eventType table.
     * @return int|bool return $wpdb result  
     */
    public function insert_eventType( $values )
    {
        $table_name = $this->wpPrefix . $this->prefix . "eventType";
        return $this->insert_query( $table_name, $this->eventType_parameters, $values );
    }

    /**
     * Insert data into event table.
     * @param array $values Values to insert into the event table.
     * @return int|bool return $wpdb result  
     */
    public function insert_event( $values )
    {
        $table_name = $this->wpPrefix . $this->prefix . "event";
        return $this->insert_query( $table_name, $this->event_parameters, $values );
    }

    /**
     * Insert data into mec_events mec table.
     * @param array $values Values to insert into the mec_events table.
     * @return int|bool return $wpdb result  
     */
    public function insert_mec_events( $arrayofevents )
    {        
        global $wpdb; 
        $table_name = $this->wpPrefix . "mec_events";
        $values = array();
        foreach ($arrayofevents as $row){

            $post_id = $row[0];
            $items = $row[1];

                $value = $wpdb->prepare( 
                    "(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", 
                    $items[0], 
                    $items[1],
                    $items[2],
                    $items[3],
                    $items[4], 
                    $items[5],
                    $items[6],
                    $items[7], 
                    $items[8],
                    $items[9],
                    $items[10],
                    $items[11], 
                    $items[12],
                    $items[13],
                    $items[14],
                    $items[15]
                );
                $values[] = $value;
        }
        $parameters = "`". implode( "`,`", $this->mec_events_parameters). "`";
        $query = "INSERT INTO ". $table_name ." (".$parameters.") VALUES ";
        $query .= implode( ",\n", $values );
       
        return  $wpdb->query( $query );
    }

    /**
     * Insert data into mec_dates mec table.
     * @param array $values Values to insert into the mec_dates table.
     * @return int|bool return $wpdb result  
     */
    public function insert_mec_dates( $arrayofdates )
    {
        
        global $wpdb; 
        $table_name = $this->wpPrefix . "mec_dates";
        $values = array();

        foreach ($arrayofdates as $row){

            $post_id = $row[0];
            $items = $row[1];           

            $value = $wpdb->prepare( "(%s,%s,%s,%s,%s,%s)",$items[0], $items[1],$items[2],$items[3],$items[4],$items[5] );
            $values[] = $value;
        }
        $query = "INSERT INTO ". $table_name ." ( id,post_id ,dstart,dend,tstart,tend) VALUES ";
        $query .= implode( ",\n", $values );
        return  $wpdb->query( $query );
    }

    /**
     * Insert posts meta in the database
     * @param array $values Values to insert into the table.
     * @return int|bool return $wpdb result  
     */
    public function posts_meta_insert( $arrayofmeta )
    {
        global $wpdb; 
        $table_name = $this->wpPrefix . "postmeta";
        $values = array();

        foreach ($arrayofmeta as $row){

            $post_id = $row[0];
            $items = $row[1];
            
            foreach ( $items as $key => $value ) {

                if ( is_array ( $value ) ){
                    $value = serialize ( $value );
                }
                
                $value = $wpdb->prepare( "(%s,%s,%s)", $post_id , $key,  $value );
                $values[] = $value;
            }
        }
        $query = "INSERT INTO ". $table_name ." ( post_id, meta_key ,meta_value ) VALUES ";
        $query .= implode( ",\n", $values );
        
        return  $wpdb->query( $query );
    }

    /**
     * Insert data into wp_terms_relationships mec table.
     * @param array $values Values to insert into wp_terms_relationships table.
     * @return int|bool return $wpdb result  
     */
    public function insert_term_relationships( $values )
    {
        $table_name = $this->wpPrefix . "term_relationships";
        return $this->insert_query( $table_name, $this->term_relationships_parameters, $values );
    }

     /**
     * Insert into relationship between eventType and category tables.
     * @param array $values Values to insert into the table.
     * @return int|bool return $wpdb result  
     */
    public function insert_eventType_category( $values )
    {
        $table_name = $this->wpPrefix . $this->prefix . "eventType_category";
        return $this->insert_query( $table_name, $this->eventType_category_parameters, $values );
    }

    /**
     * Insert into seasons table.
     * @param array $values Values to insert into the table.
     * @return int|bool return $wpdb result  
     */
    public function insert_seasons( $values )
    {
        $table_name = $this->wpPrefix . $this->prefix . "season";
        return $this->insert_query( $table_name, $this->season_parameters, $values );
    }

    /**
     * Insert into leagues table.
     * @param array $values Values to insert into the table.
     * @return int|bool return $wpdb result  
     */
    public function insert_leagues( $values )
    {
        $table_name = $this->wpPrefix . $this->prefix . "league";
        return $this->insert_query( $table_name, $this->league_parameters, $values );
    }

    /**
     * Insert into teams table.
     * @param array $values Values to insert into the table.
     * @return int|bool return $wpdb result  
     */
    public function insert_teams( $values )
    {
        $table_name = $this->wpPrefix . $this->prefix . "team";
        return $this->insert_query( $table_name, $this->team_parameters, $values );
    }

    /**
     * Insert into wordPress custom post table.
     * @param array $values Values to insert into the custom post table.
     * @return int|bool return $wpdb result  
     */
    public function insert_post( $values )
    {
        return wp_insert_post( $values );
    }

    /**
     * Insert into wordPress custom post table usin sql query.
     * @param array $values Values to insert into the custom post table.
     * @return int|bool return $wpdb result  
     */
    public function insert_posts_sql( )
    {
       
       $guid = get_site_url() . "/" ."events-calendar" . "/" ;
       $query  = "
                    INSERT INTO `". $this->wpPrefix ."posts`
                    (
                    `ID`,
                    `post_author`,
                    `post_date`,
                    `post_date_gmt`,
                    `post_content`,
                    `post_title`,
                    `post_excerpt`,
                    `post_status`,
                    `comment_status`,
                    `ping_status`,
                    `post_password`,
                    `post_name`,
                    `to_ping`,
                    `pinged`,
                    `post_modified`,
                    `post_modified_gmt`,
                    `post_content_filtered`,
                    `post_parent`,
                    `guid`,
                    `menu_order`,
                    `post_type`,
                    `post_mime_type`,
                    `comment_count`)
                    
                    SELECT 
                    '' AS ID,
                    '". get_current_user_id() . "' AS `post_author`,
                    NOW() AS `post_date`, 
                    UTC_TIMESTAMP() as `post_date_gmt`, 
                    content AS `post_content`, 
                    post_title AS `post_title`, 
                    '' AS `post_excerpt`, 
                    'publish' AS `post_status`, 
                    'open' AS `comment_status`, 
                    'closed' AS `ping_status`, 
                    '' AS `post_password`,
                    CONCAT( SUBSTRING_INDEX( description,'%%',-1 ),'-', eventId ) AS `post_name`,
                    '' AS `to_ping`,
                    '' AS `pinged`,
                    NOW() AS `post_modified`, 
                    UTC_TIMESTAMP() AS `post_modified_gmt`, 
                    '' AS `post_content_filtered`,
                    0 AS `post_parent`,
                    CONCAT('". $guid."', CONCAT( SUBSTRING_INDEX( description,'%%',-1 ),'-', eventId ),'/') AS `guid`,
                    0 AS `menu_order`,
                    'mec-events' AS `post_type`,
                    '' AS `post_mime_type`,
                    0 AS `comment_count`
                    FROM (
 
                        SELECT k.*,
                        CASE  
                        WHEN k.eventTypeCode = 'c' AND (description = 'Level 1%%level-1' or description = 'Level 2%%level-2' or description = 'Level 3%%level-3') THEN CONCAT(l.leagueName,' ', l.teamName)
                        ELSE  SUBSTRING_INDEX( description,'%%',1 )
                        END AS post_title,
                        TRIM( l.leagueInfo ) as content 

                        FROM  (SELECT i.*,j.name AS resourceName FROM `".$this->wpPrefix."".$this->prefix."event` AS i, `".$this->wpPrefix."".$this->prefix."resource` AS j WHERE i.resourceId = j.resourceId  ) AS k 
                        LEFT JOIN (
                                    SELECT m.teamId, m.name AS teamName, n.leagueName, n.leagueInfo, n.seasonName 
                                    FROM  `".$this->wpPrefix."".$this->prefix."team` AS m
                                    LEFT JOIN (  SELECT x.leagueId, x.name AS leagueName, x.info AS leagueInfo, y.name AS seasonName 
                                                 FROM  `".$this->wpPrefix."".$this->prefix."league`  AS x
                                                 LEFT JOIN `".$this->wpPrefix."".$this->prefix."season` AS y  
                                                 ON  x.seasonId = y.seasonId
                                                 GROUP BY x.leagueId) AS n             
                                    ON m.leagueId = n.leagueId
                                    GROUP BY m.teamId
                                ) AS l
                        ON k.hteamId = l.teamId
                        GROUP BY k.eventId ) AS z;
                ";
       
       
        global $wpdb;    
        return $wpdb->query(  $query  ); 
    }

     /**
     * Insert into wordPress custom post table usin sql query.
     * @param array $values Values to insert into the custom post table.
     * @return int|bool return $wpdb result  
     */
    public function assign_post_category( )
    {
       
       $query  = "INSERT INTO `". $this->wpPrefix ."term_relationships`
                    (`object_id`,
                    `term_taxonomy_id`,
                    `term_order`)
                    
                    SELECT
                    a.ID AS `object_id`, 
                    b.mec_category_taxonomy_term_id AS `term_taxonomy_id`,0 AS `term_order` 
                    FROM ". $this->wpPrefix ."posts AS a, (
                                            SELECT eventId, description, y.mec_category_taxonomy_term_id  
                                            FROM 
                                            ". $this->wpPrefix . $this->prefix. "event as x, 
                                            ". $this->wpPrefix . $this->prefix. "eventType_category as y 
                                            WHERE  x.eventTypeCode = y.eventTypeCode
                                            ) AS b 
                    WHERE  a.post_name = concat( SUBSTRING_INDEX( b.description,'%%',-1 ),'-', b.eventId )
                    GROUP BY a.ID
                    ORDER BY ID ASC
                ";
       
       
        global $wpdb;
        return $wpdb->query(  $query  ); 
    }

    /**
     * Assign the post a location.
     * @param array $values Values to insert into the custom post table.
     * @return int|bool return $wpdb result  
     */
    public function assign_post_location( )
    {
       
       $query  = "INSERT INTO `". $this->wpPrefix ."term_relationships`
                    (`object_id`,
                    `term_taxonomy_id`,
                    `term_order`)
                    
                    SELECT
                        i.ID AS `object_id`, 
                        j.mecLocationId AS `term_taxonomy_id`,
                        0 AS `term_order`
                        FROM ".$this->wpPrefix."posts AS i, (  select a.eventId, a.start, a.end, a.description, a.eventTypeCode, a.resourceId, a.facilityId, a.mecLocationId , b.hexColour   
                                                FROM
                                                    ( 
                                                        SELECT x.*, y.mecLocationId , y.facilityId
                                                        FROM `".$this->wpPrefix."".$this->prefix."event` AS x
                                                        left  JOIN ( SELECT m.resourceId, mecLocationId, m.facilityId 
                                                                    FROM `".$this->wpPrefix."".$this->prefix."resource` AS m, `".$this->wpPrefix."".$this->prefix."facility` AS n
                                                                    WHERE  m.facilityId = n.facilityId
                                                                    GROUP BY m.resourceId
                                                                    ) AS y 
                                                                    ON x.resourceId = y.resourceId 
                                                                    
                                                        ) AS a
                                                        LEFT JOIN `".$this->wpPrefix."".$this->prefix."eventType` AS b
                                                        ON a.eventTypeCode = b.code
                                                        GROUP BY a.eventId
                                                        ORDER BY a.eventId ASC) AS j

                        WHERE  i.post_name = concat( SUBSTRING_INDEX( j.description,'%%',-1 ),'-', j.eventId ) 
                        GROUP BY i.ID
                        ORDER BY i.ID ASC;
                ";
       
       
        global $wpdb;
        return $wpdb->query(  $query  ); 
    }

    /**
     * Truncate facility table.
     * @return int|bool return $wpdb result  
     */
    public function truncate_facility_table()
    {
        $table_name = $this->wpPrefix . $this->prefix . "facility";
        return $this->truncate_query( $table_name );
    }

    /**
     * Truncate resource table.
     * @return int|bool return $wpdb result  
     */
    public function truncate_resource_table()
    {
        $table_name = $this->wpPrefix . $this->prefix . "resource";
        return $this->truncate_query( $table_name );
    }

    /**
     * Truncate eventType table.
     * @return int|bool return $wpdb result  
     */
    public function truncate_eventType_table()
    {
        $table_name = $this->wpPrefix . $this->prefix . "eventType";
        return $this->truncate_query( $table_name );
    }

    /**
     * Truncate eventType_category table.
     * @return int|bool return $wpdb result  
     */
    public function truncate_eventType_category_table()
    {
        $table_name = $this->wpPrefix . $this->prefix . "eventType_category";
        return $this->truncate_query( $table_name );
    }

    /**
     * Truncate event table.
     * @return int|bool return $wpdb result  
     */
    public function truncate_event_table()
    {
        $table_name = $this->wpPrefix . $this->prefix . "event";
        return $this->truncate_query( $table_name );
    }

    /**
     * Truncate season table.
     * @return int|bool return $wpdb result  
     */
    public function truncate_season_table()
    {
        $table_name = $this->wpPrefix . $this->prefix . "season";
        return $this->truncate_query( $table_name );
    }

    /**
     * Truncate league table.
     * @return int|bool return $wpdb result  
     */
    public function truncate_league_table()
    {
        $table_name = $this->wpPrefix . $this->prefix . "league";
        return $this->truncate_query( $table_name );
    }

    /**
     * Truncate team table.
     * @return int|bool return $wpdb result  
     */
    public function truncate_team_table()
    {
        $table_name = $this->wpPrefix . $this->prefix . "team";
        return $this->truncate_query( $table_name );
    }

    /**
     * Truncate mec_events table.
     * @return int|bool return $wpdb result  
     */
    public function truncate_mec_events_table()
    {
        $table_name = $this->wpPrefix  . "mec_events";
        return $this->truncate_query( $table_name );
    }

    /**
     * Truncate mec_dates table.
     * @return int|bool return $wpdb result  
     */
    public function truncate_mec_dates_table()
    {
        $table_name = $this->wpPrefix . "mec_dates";
        return $this->truncate_query( $table_name );
    }
    
    /**
     * Get mec_event table maxID.
     * @return int return max ID of the wordpress table;  
     */
    public function mec_event_table_max_id()
    {
        $table = $this->wpPrefix . "mec_events";
        return $this->get_table_max_id( $table );    
    }

    /**
     * Get WordPress table maxID.
     * @return int return max ID of the wordpress table;  
     */
    public function get_table_max_id( $table )
    {
        global $wpdb;
        $query = "SELECT ID FROM `". $table ."` ORDER BY ID DESC LIMIT 0,1;";       
        $max_id = $wpdb->get_var(  $query  );

        return ( int ) $max_id;        
    }

    /**
     * Get rows of event table.
     * @return int return rows of event table;  
     */
    public function select_event_rows_asc ( $eventTypeCode )
    {
        $event = $this->wpPrefix . $this->prefix . 'event';
        $resource = $this->wpPrefix . $this->prefix . 'resource';
        $facility = $this->wpPrefix . $this->prefix . 'facility'; 
        $posts = $this->wpPrefix . 'posts';   
        global $wpdb;
        $query = "
        SELECT
        i.ID AS `postId`, 
        j.eventId AS `eventId`,
        j.hteamId AS `hteamId`,
        i.post_content AS `description`,
        i.guid AS `guid`,
        j.mecLocationId AS `mecLocationId`,
        j.start AS `start`,
        j.end AS `end` 
        FROM `". $posts ."` AS i, ( SELECT x.*, y.mecLocationId , y.facilityId
								FROM (select * from `". $event ."` WHERE eventTypeCode = '".$eventTypeCode."') AS x
								left  JOIN ( 
											SELECT m.resourceId, mecLocationId, m.facilityId 
											FROM `". $resource ."` AS m, `". $facility ."` AS n
											WHERE  m.facilityId = n.facilityId
											GROUP BY m.resourceId
										   ) AS y 
											ON x.resourceId = y.resourceId
								) AS j
        
        WHERE  i.post_name = concat( SUBSTRING_INDEX( j.description,'%%',-1 ),'-', j.eventId ) 
        GROUP BY i.ID
        ORDER BY i.ID ASC;
        ";
        return $wpdb->get_results(  $query  );                 
    }

    
    /**
     * Get rows of the event type table.
     * @return array return array of rows of the table;  
     */
    public function select_evenType_list()
    {
        $table = $this->wpPrefix . $this->prefix . "eventType";
        return $this->select_table_rows( $table );        
    }

    /**
     * Get rows of a table.
     * @return int return max ID of the wordpress posts table;  
     */
    public function select_table_rows ( $table, $conditionals ="" )
    {
        global $wpdb;
        $query = "SELECT * FROM `". $table ."` ". $conditionals .";"; 
        return $wpdb->get_results(  $query  );        
    }

    /** 
     * Prepare SQL query to create database table
     * @param string $prefix wordpress tables prefix.
     * @access public
     */  
    public static function create_tables( $prefix )
    {

        $querys[]  = 'CREATE TABLE IF NOT EXISTS `' . 
                        $prefix. DASH_SYNC_DB_PREFIX .'facility` (
                        `location` VARCHAR(100) NULL,
                        `facilityId` INT NULL,
                        `mecLocationId` INT NULL);                            
                     '; 

        $querys[]  = 'CREATE TABLE IF NOT EXISTS `' . 
                        $prefix. DASH_SYNC_DB_PREFIX .'resource` (
                        `resourceId` INT NULL,
                        `name` VARCHAR(100) NULL,
                        `facilityId` VARCHAR(45) NULL);
                     '; 
        
        $querys[]  = 'CREATE TABLE IF NOT EXISTS `' . 
                        $prefix. DASH_SYNC_DB_PREFIX .'eventType` (
                        `eventTypeId` INT NULL,
                        `code` VARCHAR(3) NULL,
                        `name` VARCHAR(100) NULL,
                        `hexColour` VARCHAR(7) NULL,
                        `apiCall` CHAR NULL);
                     ';

        $querys[]  = 'DROP TABLE IF EXISTS `' . $prefix . DASH_SYNC_DB_PREFIX .'event`;';

        $querys[]  = 'CREATE TABLE IF NOT EXISTS `' . 
                        $prefix. DASH_SYNC_DB_PREFIX .'event` (
                        `eventId` INT NULL,
                        `hteamId` INT NULL,
                        `start` DATETIME(0) NULL,
                        `end` DATETIME(0) NULL,
                        `description` VARCHAR(100) NULL,
                        `eventTypeCode` VARCHAR(3) NULL,
                        `resourceId` INT NULL);
                    ';

        $querys[]  = 'CREATE TABLE IF NOT EXISTS `' .
                             $prefix. DASH_SYNC_DB_PREFIX .'eventType_category` (
                            `eventTypeCode`             varchar(3)  DEFAULT NULL,
                            `mec_category_taxonomy_term_id` int(11) DEFAULT NULL
                        );
                    ';

        $querys[]  = 'CREATE TABLE IF NOT EXISTS `' .
                             $prefix. DASH_SYNC_DB_PREFIX .'season`  (
                            `seasonId` INT NULL,
                            `name` VARCHAR(100) NULL,
                            `start` DATETIME(0) NULL,
                            `end` DATETIME(0) NULL,
                            `facilityId` INT NULL
                        );
                    ';

        $querys[]  = 'DROP TABLE IF EXISTS `' . $prefix . DASH_SYNC_DB_PREFIX .'league`;';
        
        $querys[]  = 'CREATE TABLE IF NOT EXISTS `' .
                             $prefix. DASH_SYNC_DB_PREFIX .'league`   (
                            `leagueId` INT NULL,
                            `name` VARCHAR(100) NULL,
                            `info` VARCHAR(200) NULL,
                            `start` DATETIME(0) NULL,
                            `seasonId` INT NULL,
                            `facilityId` INT NULL
                        );
                    ';
        
        $querys[]  = 'CREATE TABLE IF NOT EXISTS `' .
                             $prefix. DASH_SYNC_DB_PREFIX .'team` (
                            `teamId` INT NULL,
                            `name` VARCHAR(100) NULL,
                            `start` DATETIME(0) NULL,
                            `leagueId` INT NULL,
                            `seasonId` INT NULL,
                            `facilityId` INT NULL
                        );
                    ';

        global $wpdb; 

        foreach ( $querys as $query ){
           $wpdb->query( $query );
        }      
    }

    /** 
     * Prepare insert SQL query to insert data in the specified table.
     * @param string    $table_name Name of the the table to insert data.
     * @param array     $parameters Parameters of the table
     * @param array     $values to insert in each parameter
     * @return int|bool return $wpdb result      
     * @access public   
     */ 
    public function insert_query( $table_name, $parameters, $values )
    {

        $parameters = $this->format_parameters($parameters);
        $values = $this->format_values( $values );
        $query = "INSERT INTO `". $table_name ."` (". $parameters .") VALUES (". $values .")";
        
        global $wpdb; 
        
        return  $wpdb->query( $query );
    }

     /** 
     * Prepare table Truncate SQL query to remove data in the specified table.
     * @param string    $table_name Name of the the table to Truncate data.
     * @return int|bool return $wpdb result      
     * @access public   
     */ 
    public function truncate_query( $table_name )
    {

        $query = "Truncate table `". $table_name."`";
        
        global $wpdb; 
        
        return  $wpdb->query( $query );
    }

    /** 
     * Delete mec data from PostMeta table.   
     * @access public   
     */ 
    public function delete_postmeta_rows_query()
    {

        $query = "DELETE from `". $this->wpPrefix . "postmeta` 
                  where post_Id in (select post_id 
                                    from `". $this->wpPrefix . "posts` 
                                    where post_type = 'mec-events')";
        
        global $wpdb; 
        
        return  $wpdb->query( $query );
    }

    /** 
     * Delete mec data from Posts table.   
     * @access public   
     */ 
    public function delete_posts_rows_query()
    {
 
        $query = "DELETE FROM `". $this->wpPrefix ."posts` where post_type = 'mec-events'";
        
        global $wpdb; 
        
        return  $wpdb->query( $query );
    }

    /** 
     * Format the paramaters before be inserted in the database
     * @param array    $parameters Parameters of the table.
     * @return string  Formated parameters to be used in the query.
     */ 
    private function format_parameters( $parameters )
    {
        return "`". implode( "`,`", $parameters ) ."`";
    }

    /** 
     * Format the paramaters before be inserted in the database.
     * @param array    $values Values to be inserted in the table.
     * @return string  Formated values to be used in the query.
     */ 
    private function format_values( $values )
    {
        $format_values = [];
        foreach ($values as $value) {
            $format_values = esc_sql ( $values );# code...
        }
        return "'". implode( "','", $format_values ) ."'";
    }

    /** 
     * Delete temporal data from the databasa.
     * @param string $prefix wordpress tables prefix.
     * @param string $table_name name of the table to drop.
     * @access public
     */  
    public function delete_post_data( $table_name, $parameter )
    {
        global $wpdb;   
        $query = "
        
        DELETE  " . $table_name . "
        FROM  " . $table_name . " 
        WHERE 
        " . $parameter . " in (
			 SELECT post_id 
			 FROM " . $this->wpPrefix . "postmeta 
			 WHERE meta_key = 'dash_temp' and meta_value = 1);
        ";  
        return $wpdb->query( $query );
    }

    /** 
     * Delete orphan postmeta of the database.
     * @param string $prefix wordpress tables prefix.
     * @param string $table_name name of the table to drop.
     * @access public
     */  
    public function delete_postmeta_data( )
    {
        global $wpdb;   
        $query = "
        DELETE FROM " . $this->wpPrefix . "postmeta 
        WHERE post_id NOT IN (SELECT ID FROM " . $this->wpPrefix . "posts);
        ";  
        return $wpdb->query( $query );
    }

    /** 
     * Delete orphan post without postmeta int the database.
     * @param string $prefix wordpress tables prefix.
     * @param string $table_name name of the table to drop.
     * @access public
     */  
    public function delete_orphan_posts()
    {
        global $wpdb;   
        $query = "
        DELETE FROM " . $this->wpPrefix . "posts 
        WHERE  post_type = 'mec-events' and ID 
        not in (select post_id from " . $this->wpPrefix . "postmeta GROUP BY post_id );
        ";  
        return $wpdb->query( $query );

    }

    /** 
     * Drop one table from database.
     * @param string $table Name of the table to drop.
     * @access public
     */  
    public static function drop_table( $table )
    {
        global $wpdb;     
        $wpdb->query( "DROP TABLE " . $table . ";" );
    }

}