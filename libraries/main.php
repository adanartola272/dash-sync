<?php
/**
 * @author Whyte Enterprises
 * @package  DASH-Sync 
 */

/**
 * Receptor class
 */
class DASHSync {

	/**
	 * The factory is responsible of register actions and filters
	 * @access   protected
	 * @var      FileReader    $loader    Maintains and registers all hooks for the plugin.
	 */
	 protected $file_reader;

	/**
	 * The factory is responsible of register actions and filters
	 * @access   protected
	 * @var      factory    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $factory;

	/**
	 * Request data from the dash API
	 * @access   protected
	 * @var      receiver    $receiver    
	 */
	protected $receiver;

	/**
	 * Writes events to the log file 
	 * @access   protected
	 * @var      logger    $logger    
	 */
	protected $logger;

	/**
	 * The unique identifier of this plugin.
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;


	public function __construct() 
	{

		if ( defined( 'DASH_SYNC_VERSION' ) ) {
			$this->version = DASH_SYNC_VERSION;
		} else {
			$this->version = '0.0';
		}

		$this->plugin_name = 'DASH-Sync';
		
		try{			
			$this->load_libraries();
		} catch( Exception $e ){
			$event = array( 'error' => true, 'exception'  => $e->getMessage() );
			$this->logger-> write_to_Log( $event );
        }
	}

	/**
	 * RUN the plugin.
	 * @access public
	 */

	public function run() {
		//set_time_limit ( 0 );
		 
		$event =  array( 'error' => false, 'exception'  =>'Data synchronization started.' );
	    $this->logger-> write_to_Log( $event );

		$is_clean            = $this->clean_database();
		$is_loaded           = $this->load_config_files();				
		$resources_collected = $this->collect_resources();			
		$events_collected    = $this->collect_events();			
		$seasons_collected   = $this->collect_seasons();
		$leagues_collected   = $this->collect_leagues();
		$teams_collected     = $this->collect_teams();
		
		
		if ($is_clean and 
			$is_loaded and 
			$resources_collected and  
			$events_collected and
			$seasons_collected and
			$leagues_collected and
			$teams_collected){
			
			$posts_deleted = $this->delete_old_posts();		
			
			$posts_updated = $this->syncronize_wpposts_mec_tables();	
		
			
			if ( $posts_deleted and $posts_updated ) {
				$event =  array( 'error' => false, 'exception'  =>'Data synchronized succesfully.' );
				$this->logger-> write_to_Log( $event );
				return true;
			}
			
		}
		

		$event =  array( 'error' => true, 'exception'  =>'Warning: Cannot initialize DASH - Sync plugin. Please review Log.' );
		$this->logger-> write_to_Log( $event );
		return false;

	}

	
	/**
	 * Load the required dependencies for this plugin.
	 * @access private
	 */
	private function load_libraries() 
	{

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'libraries/factory.php';
		$this->factory = new Factory();
		
		 /**
         * Load class responsible of read config files
         */
        require_once plugin_dir_path(  dirname( __FILE__ ) ) . 'libraries/file_manager.php';
        $this->file_reader = new FileManager();
		
		/**
         * Load class responsible of managing the Database
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'libraries/db_manager.php';
		$this->db_manager = new DBManager();
		
		/**
         * Load class responsible request and retrieve data
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'libraries/receiver.php';
		$this->receiver = new Receiver();
		
		/**
         * Load class responsible request and retrieve data
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'libraries/logger.php';
        $this->logger = new Logger();
		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/dash_sync_admin.php';
		$admin_controller = new AdminController( );
	}

	/**
	 * Load facility.csv and even_types.csv config 
	 * files in the database.
	 * @access private
	 */
	private function load_config_files()
	{		
		$facility = $this->load_facility();
		$eventTypes = $this->load_eventTypes();
		$eventTypeCategoy = $this->load_eventType_category();

		return $facility and $eventTypes and $eventTypeCategoy;
	}

	/**
	 * Reads and inserts in the database facility.csv file data.
	 * @access private
	 */
	public function load_facility()
	{
		$facility_file = plugin_dir_path( dirname( __FILE__ ) ) . 'assets/data_files/facility.csv';
		$facility_data = $this->file_reader::read_csv_file( $facility_file );

		if ( !$facility_data["error"] ) {

			foreach ( $facility_data["data"] as $row ) {
				$this->db_manager->insert_facility( $row );
			}

		} else {

			$this->logger-> write_to_Log( $facility_data );
			
			return false;
		}
		return true;
	}

	/**
	 * Reads and inserts in the database evenTypes.csv file data.
	 * @access private
	 */
	public function load_eventTypes()
	{
		$event_types_file = plugin_dir_path( dirname( __FILE__ ) ) . 'assets/data_files/eventTypes.csv';

		$event_types_data = $this->file_reader::read_csv_file( $event_types_file );

		if ( !$event_types_data["error"] ) {

			foreach ( $event_types_data["data"] as $row ) {
				$this->db_manager->insert_eventType( $row );
			}

		} else {
			$this->logger-> write_to_Log( $event_types_data );
			return false;
		}

		return true;
	}

	/**
	 * Reads and inserts in the database facility.csv file data.
	 * @access private
	 */
	public function load_eventType_category()
	{
		$category_file = plugin_dir_path( dirname( __FILE__ ) ) . 'assets/data_files/eventType_category.csv';
		$category_data = $this->file_reader::read_csv_file( $category_file );

		if ( !$category_data["error"] ) {

			foreach ( $category_data["data"] as $row ) {
				$this->db_manager->insert_eventType_category( $row );
			}

		} else {
			$this->logger-> write_to_Log( $category_data );
			return false;
		}

		return true;

	}


	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 * @access private
	 */
	private function define_hooks() 
	{
	
		$admin_controller = new AdminController( );

		$this->factory->add_action( 'admin_notices', $admin_controller, 'update_notice' );
		

    }
    
	/**
	 * Execute factory and register actions and filters in wordpress with WordPress.
	 */
	public function execute() 
	{
		$this->factory->register();
	}

	/**
	 * Request and inserts in the database resources data.
	 * @access public
	 */
	public function collect_resources()
	{
		$response = $this->receiver->token_ready();
		if ( !$response['error'] ){

			$page_size = 1000;
			$resources = $this->receiver->resources_data_request( $page_size );
			$page_items =  ( array ) $resources['response']->meta->page;
			$current_page = $page_items["current-page"];			
			$last_page = $page_items["last-page"];

			while ( $current_page <= $last_page ){
				
				$current_page += 1;				
				$data = ( array ) $resources["response"]->data;			
				
				foreach ( $data as $row ){
					$insert_data = array(
						$row->id,
						$row->attributes->name,
						$row->attributes->{"facility_id"},
					);

					$this->db_manager->insert_resource( $insert_data );
					
				}
				$resources = $this->receiver->resources_data_request( $page_size, $current_page );
			}
						
		}else{			
			$this->logger-> write_to_Log( $response );
			return false;
		}
		return true;
	}

	/**
	 * Request and inserts in the database temporary tables of all events data.
	 * @access public
	 */
	public function collect_events()
	{
		try{

		$events = $this->db_manager->select_evenType_list();
			foreach ( $events  as $event ){
				if ( strcmp( $event->apiCall, "N" ) != 0 ) {
					$response = $this->collect_event_type( $event->code );
				} 
			}
		} catch( Exception $e ){
			$event = array( 'error' => true, 'exception'  => $e->getMessage() );
			$this->logger-> write_to_Log( $event );
			return false;
		}

		return true;

	}
	/**
	 * Request and inserts in the database events data by event type. 
	 * @param string $eventType Event type of the event.
	 * @access public
	 */
	public function collect_event_type( $eventType = "10" )
	{
		$response = $this->receiver->token_ready();
		if ( !$response['error']){
			$page_size    = 3000;
			
			$current_page = 1;
			$response     = $this->receiver->events_data_request( $page_size, $current_page, $eventType );			
		
			$page_items   = $response['response']->meta->page;								
			$last_page    = $page_items->{ "last-page" };
			$total        = $page_items->{ "total" };
			
			while ( ( $current_page <= $last_page ) and $total>0 ){
				
				$current_page += 1;				
				$data =  $response["response"]->data;
								
				foreach ( $data as $row ){

					$team = DASH_SYNC_NULL_FLAG;
					if ( !is_null( $row->attributes->{"hteam_id"} ) ){
					 	$team = $row->attributes->{"hteam_id"};
					}
					
					$insert_data = array(
						$row->{"id"},
						$team,
						$row->attributes->{"start"},
						$row->attributes->{"end"},
						$row->attributes->{"desc"}. "%%" . $this->post_title_format( $row->attributes->desc ),
						$row->attributes->{"event_type"},
						$row->attributes->{"resource_id"},
					);
					
					$this->db_manager->insert_event( $insert_data );				
				}

				$response = $this->receiver->events_data_request( $page_size, $current_page, $eventType );
				
			}						
		} else {
			
			$this->logger-> write_to_Log( $response );
			return false;
		}
		return true;
	}

	/**
	 * Request and inserts in the database, seasons data.
	 * @access public
	 */
	public function collect_seasons()
	{
		$response = $this->receiver->token_ready();
		if ( !$response['error'] ){

			$page_size = 1000;
			$current_page = 1;
			$seasons = $this->receiver->seasons_data_request( $page_size, $current_page );
			$page_items =  ( array ) $seasons['response']->meta->page;		
			$last_page = $page_items["last-page"];

			while ( $current_page <= $last_page ){
				
				$current_page += 1;				
				$data = ( array ) $seasons["response"]->data;			
				
				foreach ( $data as $row ){
					$insert_data = array(
						$row->{"id"},
						$row->attributes->{"name"},
						$row->attributes->{"start_date"},
						$row->attributes->{"end_date"},
						$row->attributes->{"facility_id"},
					);

					$this->db_manager->insert_seasons( $insert_data );
					
				}
				$seasons = $this->receiver->seasons_data_request( $page_size, $current_page );
			}
						
		}else{			
			$this->logger-> write_to_Log( $response );
			return false;
		}
		return true;
	}

	/**
	 * Request and inserts in the database, leagues data.
	 * @access public
	 */
	public function collect_leagues()
	{
		$response = $this->receiver->token_ready();
		if ( !$response['error'] ){

			$page_size = 1000;
			$current_page = 1;
			$leagues = $this->receiver->leagues_data_request( $page_size, $current_page );
			$page_items =  ( array ) $leagues['response']->meta->page;		
			$last_page = $page_items["last-page"];

			while ( $current_page <= $last_page ){
				
				$current_page += 1;				
				$data = ( array ) $leagues["response"]->data;			
				
				foreach ( $data as $row ){
					$insert_data = array(
						$row->{"id"},						
						$row->attributes->{"name"},
						$row->attributes->{"info"},
						$row->attributes->{"start_date"},
						$row->attributes->{"season_id"},
						$row->attributes->{"facility_id"},
					);

					$this->db_manager->insert_leagues( $insert_data );
					
				}
				$leagues = $this->receiver->leagues_data_request( $page_size, $current_page );
			}
						
		}else{			
			$this->logger-> write_to_Log( $response );
			return false;
		}
		return true;
	}

	/**
	 * Request and inserts in the database, teams data.
	 * @access public
	 */
	public function collect_teams()
	{
		$response = $this->receiver->token_ready();
		if ( !$response['error'] ){

			$page_size = 1000;
			$current_page = 1;
			$teams = $this->receiver->teams_data_request( $page_size, $current_page  );
			$page_items =  ( array ) $teams['response']->meta->page;		
			$last_page = $page_items["last-page"];

			while ( $current_page <= $last_page ){
				
				$current_page += 1;				
				$data = ( array ) $teams["response"]->data;			
				
				foreach ( $data as $row ){
					$insert_data = array(
						$row->{"id"},
						$row->attributes->{"name"},
						$row->attributes->{"start_date"},
						$row->attributes->{"league_id"},
						$row->attributes->{"season_id"},
						$row->attributes->{"facility_id"},
					);

					$this->db_manager->insert_teams( $insert_data );
					
				}
				$teams = $this->receiver->teams_data_request( $page_size, $current_page );
			}
						
		}else{			
			$this->logger-> write_to_Log( $response );
			return false;
		}
		return true;
	}

	/**
	 * Clean database of previous posts, postmeta, mec_events, mec_dates.
	 * @return int|bool return $wpdb result  
	 */

	public function clean_database()
	{
		try{
			
			$this-> db_manager->truncate_facility_table();
			$this-> db_manager->truncate_resource_table();
			$this-> db_manager->truncate_eventType_category_table();
			$this-> db_manager->truncate_eventType_table();
			$this-> db_manager->truncate_event_table();
			$this-> db_manager->truncate_season_table();
			$this-> db_manager->truncate_league_table();
			$this-> db_manager->truncate_team_table();

		}catch( Exception $e ){
			$event = array( 'error' => true, 'exception'  => $e->getMessage() );
			$this->logger-> write_to_Log( $event );
			return false;
		}
		return true;
	}

	/**
	 * Syncronize api events in wordpress tables posts, postmeta, 
	 * and caledar tables mec_events, mec_dates.
	 * @return int|bool return $wpdb result  
	 */
	public function syncronize_wpposts_mec_tables_php()
	{
		
		$event_table_data = $this->db_manager->select_event_rows_asc();
		$date_id_prefix = date("YmdHi");
		$ids = -999;
		

		foreach ( $event_table_data  as $event ){
			set_time_limit(1);
			$ID = $date_id_prefix . $event->{ "eventId" };
			$post_title = $event->{ "description" };
			$date_start = $event->{ "start" };
			$date_end = $event->{ "end" };
			$eventType_color = $event->{ "hexColour" };
			$facilityId = $event->{ "facilityId" };
			$post_name = $this->post_title_format( $post_title ) . "-" . $event->{ "eventId" };
			$guid = get_site_url() . "/" . 
					"events-calendar" . "/" . 
					$post_name;

			$meta = $this->format_mec_postmeta( 
				$event->{ "eventId" }, 
				$eventType_color, 
				$facilityId,
				$date_start, 
				$date_end,$guid );

			$insert_data  = array (
				
				'post_title'   => $post_title,
				'post_content' => $post_title,
				'post_status'  => 'publish',
				'post_name'	   => $post_name,
				'post_author'  => get_current_user_id(),
				'guid'		   => $guid,
				'post_type'    => 'mec-events',
				//'meta_input'   => $meta,
			);
			
			$ids = $this->db_manager->insert_post( $insert_data );
			

			$insert_data_relationship = array( $ids, 6, 0);
			$this->db_manager->insert_term_relationships ( $insert_data_relationship );

			//$insert_data_relationship = array( $ids, 3, 0, );
			//$this->db_manager->insert_term_relationships ( $insert_data_relationship );
			

			$insert_data_mec_events = $this->format_mec_events( $ids, $date_start, $date_end );			
			$this->db_manager->insert_mec_events( $insert_data_mec_events );

			$insert_data_mec_dates = $this->format_mec_dates( $ids, $date_start, $date_end );			
			$this->db_manager->insert_mec_dates( $insert_data_mec_dates );
		}
	}

	/**
	 * Syncronize api events in wordpress tables posts, postmeta, 
	 * and caledar tables mec_events, mec_dates.
	 * @return int|bool return $wpdb result  
	 */
	public function syncronize_wpposts_mec_tables()
	{	
		try {	
			
			$this->db_manager->insert_posts_sql();
			$this->db_manager->assign_post_category();
			$this->db_manager->assign_post_location();			
			$this->syncronize_posts_meta_mec_tables();
			
			
		}catch( Exception $e ){
			$event = array( 'error' => true, 'exception'  => $e->getMessage() );
			$this->logger-> write_to_Log( $event );
			
			return false;
		}	
		
		return true;
	}

	/**
	 * Syncronize creates the necessary metadata to be shown 
	 * in the mec calendae.
	 * @return int|bool return $wpdb result  
	 */
	public function syncronize_posts_meta_mec_tables()
	{
		
		$eventTypes = $this->db_manager->select_evenType_list();
		
		
		foreach ( $eventTypes as $eventType ){
			
			$event_table_data = $this->db_manager->select_event_rows_asc( $eventType->{'code'} );
			$date_id_prefix = date("YmdHi");
			
			if ( sizeof( $event_table_data )>0 and strcmp( $eventType->{'apiCall'}, "N" ) != 0 ){ 

				$arrayofmeta   = array();
				$arrayofdates  = array();
				$arrayofevents = array();
				$cont = 0;

				$num_events =count($event_table_data);
				$event_count = 0;
				
				foreach ( $event_table_data  as $event ){
					
					if ( $cont < 1000 and $event_count <= $num_events-1 ){

						$post_id = $event->{ "postId" };
						$date_start = $event->{ "start" };
						$date_end = $event->{ "end" };
						$eventType_color = $eventType->{'hexColour'};
						$mecLocationId = $event->{ "mecLocationId" };
						$guid = $event->{ "guid" };

						$meta = $this->format_mec_postmeta( 
							$event->{ "eventId" },
							$event->{ "hteamId" }, 
							$eventType_color, 
							$mecLocationId,
							$date_start, 
							$date_end,
							$guid );

						$arrayofmeta[] = array($post_id,$meta);
						
						$insert_data_mec_dates = $this->format_mec_dates( $post_id, $date_start, $date_end );			
						$arrayofdates[] = array($post_id, $insert_data_mec_dates); 
						
						$insert_data_mec_events = $this->format_mec_events( $post_id, $date_start, $date_end );
						$arrayofevents[] = array($post_id, $insert_data_mec_events);
						
						$cont+=1;
						$event_count +=1;
					}else {
						$this->db_manager->posts_meta_insert( $arrayofmeta );						
						$this->db_manager->insert_mec_dates( $arrayofdates );											
						$this->db_manager->insert_mec_events( $arrayofevents );
						
						unset( $arrayofmeta );
						unset( $arrayofdates );
						unset( $arrayofevents );
						$arrayofmeta = array();
						$arrayofdates = array();
						$arrayofevents = array();
						$cont = 0;
					}
					

				}
				
				if (count($arrayofmeta) >0){
					$this->db_manager->posts_meta_insert( $arrayofmeta );						
					$this->db_manager->insert_mec_dates( $arrayofdates );											
					$this->db_manager->insert_mec_events( $arrayofevents );
					unset( $arrayofmeta );
					unset( $arrayofdates );
					unset( $arrayofevents );
				}
			}
		
		}
		
	}

	private function format_mec_dates( $post_id, $event_start, $event_end )
	{
		//$id = $this->db_manager->mec_event_table_max_id() + 1;
		$date_start = new DateTime( date( 'Y-m-d G:i', strtotime( $event_start ) ) );
		$start_date = $date_start->format('Y-m-d');

		$date_end = new DateTime( date( 'Y-m-d G:i', strtotime( $event_end ) ) );
		$end_date = $date_end->format('Y-m-d');

		$insert_data= array(
			"",
			$post_id,
			$start_date,
			$end_date,
			strtotime( $event_start ),
			strtotime( $event_end ),
		);

		return $insert_data;

	} 

	private function format_mec_events( $post_id, $event_start, $event_end  )
	{
	
		$date_start = new DateTime( date( 'Y-m-d G:i', strtotime( $event_start ) ) );
		$start_date = $date_start->format('Y-m-d');
		$start_hour = $date_start->format('g');
        $start_minutes = $date_start->format('i');
        $start_ampm = $date_start->format('A');
		$day_start_seconds = $this->time_to_seconds($this->to_24hours($start_hour, $start_ampm), $start_minutes);
		
		$date_end = new DateTime( date( 'Y-m-d G:i', strtotime( $event_end ) ) );
		$end_date = $date_end->format('Y-m-d');
		$end_hour = $date_end->format('g');
        $end_minutes = $date_end->format('i');
		$end_ampm = $date_end->format('A');
		$day_end_seconds = $this->time_to_seconds($this->to_24hours($end_hour, $end_ampm), $end_minutes);
		
		$insert_data = array(
			"",
			$post_id,
			$start_date,
			$end_date,
			0,
			NULL,NULL,NULL,NULL,NULL,NULL,NULL,
			"",
			"",
			$day_start_seconds,
			$day_end_seconds,
		);
		
		return $insert_data; 
	}

	private function format_mec_postmeta( $event_id, $hteam_id, $eventType_color,$mecLocationId, $event_start,$event_end,$guid )
	{
		
		$eventType_color = ltrim($eventType_color, "#");
		$date_start = new DateTime( date( 'Y-m-d G:i', strtotime( $event_start ) ) );

        $start_date = $date_start->format('Y-m-d');
        $start_hour = $date_start->format('g');
        $start_minutes = $date_start->format('i');
        $start_ampm = $date_start->format('A');

        $day_start_seconds = $this->time_to_seconds($this->to_24hours($start_hour, $start_ampm), $start_minutes);
            
        $date_end = new DateTime( date( 'Y-m-d G:i', strtotime( $event_end ) ) );

        $end_date = $date_end->format('Y-m-d');
        $end_hour = $date_end->format('g');
        $end_minutes = $date_end->format('i');
		$end_ampm = $date_end->format('A');
		$day_end_seconds = $this->time_to_seconds($this->to_24hours($end_hour, $end_ampm), $end_minutes);

		$mec_repeat_type = 'daily';
		$mec_repeat_interval = '1';
		$mec_repeat_advanced = '';
	    $mec_repeat_end = 'never';
		$mec_repeat_end_at_date = '';
		$mec_repeat_end_at_occurrences = '10';
			
		$mec_date = array (
			'start' => 
			array (
			  'date' => $start_date,
			  'hour' => $start_hour,
			  'minutes' => $start_minutes,
			  'ampm' => $start_ampm,
			),
			'end' => 
			array (
			  'date' => $end_date,
			  'hour' => $end_hour,
			  'minutes' => $end_minutes,
			  'ampm' => $end_ampm,
			),
			'comment' => '',
			'repeat' => 
			array (
			  'type' => $mec_repeat_type,
			  'interval' => $mec_repeat_interval,
			  'advanced' => $mec_repeat_advanced,
			  'end' => $mec_repeat_end,
			  'end_at_date' => $mec_repeat_end_at_date,
			  'end_at_occurrences' => $mec_repeat_end_at_occurrences,
			),
		);		

		$mec_repeat = array (
			'type' => $mec_repeat_type,
			'interval' => $mec_repeat_interval,
			'advanced' => $mec_repeat_advanced,
			'end' => $mec_repeat_end,
			'end_at_date' => $mec_repeat_end_at_date,
			'end_at_occurrences' => $mec_repeat_end_at_occurrences,
		);

		$more_info = '';
		if ( strcmp( $hteam_id , "-999" ) != 0 ){
			$more_info = "https://apps.dashplatform.com/dash/index.php?Action=signupSwitch/index&teamID=". $hteam_id ."&comp=canlan";
		}

		$meta = array
                    (
                        //'mec_source'=>'mec-calendar',
						//'mec_feed_event_id'=>$feed_event_id,
						'_edit_lock' =>'1580673341:3',
						'_edit_last' =>3,
						'mec_color'=> $eventType_color,
						'mec_location_id' => $mecLocationId,
						'mec_dont_show_map'=> 0,
						'mec_organizer_id'=> 1,												
                        'mec_read_more'=> $guid,//"http://icesports.com/york/camps",
                        'mec_more_info'=> $more_info,
                        'mec_more_info_title'=> "Register",
                        'mec_more_info_target'=> "_blank",
                        'mec_cost'=> "60",
						'mec_additional_organizer_ids' => array(),
						'mec_additional_location_ids' => array(),
						'mec_date'=>  $mec_date,
						'mec_repeat'=> $mec_repeat,
						'mec_certain_weekdays' => array(), 
						'mec_allday'=>0,
						'one_occurrence' =>0,
                        'mec_hide_time'=>0,
                        'mec_hide_end_time'=>0,
						'mec_comment'=>"",
						'mec_start_date'=>$start_date,
						'mec_start_time_hour'=>$start_hour,
						'mec_start_time_minutes'=>$start_minutes,
						'mec_start_time_ampm'=>$start_ampm,
						'mec_start_day_seconds'=>$day_start_seconds,
						'mec_end_date'=>$end_date,
						'mec_end_time_hour'=>$end_hour,
						'mec_end_time_minutes'=>$end_minutes,
						'mec_end_time_ampm'=>$end_ampm,
						'mec_end_time_seconds'=>$day_end_seconds,						
						'mec_repeat_status'=>0,
						'mec_repeat_type'=>$mec_repeat_type,
						'mec_repeat_interval'=>$mec_repeat_interval,
                        'mec_repeat_end'=> (string) $mec_repeat_end,
                        'mec_repeat_end_at_occurrences'=> $mec_repeat_end_at_occurrences,
						'mec_repeat_end_at_date'=> $mec_repeat_end_at_date,
						'mec_advanced_days'=> $mec_repeat_end_at_date,
                        'mec_in_days'=> "",
                        'mec_not_in_days'=> "",
                        'mec_hourly_schedules'=> array(),
                        'mec_booking'=>array(),
                        'mec_tickets'=>array(),
                        'mec_fees_global_inheritance'=> 1,
						'mec_fees'=>array(),
						'mec_ticket_variations_global_inheritance'=>1,
						'mec_ticket_variations'=>array(),
                        'mec_reg_fields_global_inheritance'=> 1,
						'mec_reg_fields'=>array(),
						'mec_op'=>array(),
						'dash_temp'=>1,
						'dash_event_id'=>$event_id,
						'_thumbnail_id'=>59,
                    );
		return $meta;
	}

	private function post_title_format( $post_title )
	{	
		$post_title = str_replace ("|","",$post_title);
		$hyphen_title = preg_replace( '#[ -]+#', '-', trim( strtolower( $post_title ) ) );
		return $this->clean_special_chars( $hyphen_title );		
	}

	private function clean_special_chars( $string_to_clean ){
		return preg_replace('/[^A-Za-z0-9\-]/', '', $string_to_clean);
	}

	/**
     * Convert a 12 hour format hour to a 24 format hour
     * @author Webnus <info@webnus.biz>
     * @param int $hour
     * @param string $ampm
     * @return int
     */
    public function to_24hours($hour, $ampm = 'PM')
    {
        $ampm = strtoupper($ampm);
        
        if($ampm == 'AM' and $hour < 12) return $hour;
        elseif($ampm == 'AM' and $hour == 12) return 24;
        elseif($ampm == 'PM' and $hour < 12) return $hour+12;
        elseif($ampm == 'PM' and $hour == 12) return 12;
        elseif($ampm == 'PM' and $hour > 12) return $hour;
	}
	
	 /**
     * Convert a certain time into seconds (Hours should be in 24 hours format)
     * @author Webnus <info@webnus.biz>
     * @param int $hours
     * @param int $minutes
     * @param int $seconds
     * @return int
     */
    public function time_to_seconds($hours, $minutes = 0, $seconds = 0)
    {
        return (($hours * 3600) + ($minutes * 60) + $seconds);
	}

	/**
	 * Delete all posts data, metada and relationships 
	 * of the database.
	 * @return int|bool return $wpdb result 
	 */	
	public function delete_old_posts()
	{
		global $wpdb;  
		$tables = array(
			$wpdb->prefix.'posts'      			=> 'ID',
			$wpdb->prefix.'mec_events' 			=> 'post_id',			
			$wpdb->prefix.'mec_dates'      		=> 'post_id',			
			$wpdb->prefix.'term_relationships'   => 'object_id',
		);

		try{		
			foreach ( $tables as $key => $value ){
				$this->db_manager->delete_post_data($key,$value);
			}
			$this->db_manager->delete_postmeta_data();
			$this->db_manager->delete_orphan_posts();
		}catch( Exception $e ){
			$event = array( 'error' => true, 'exception'  => $e->getMessage() );
			$this->logger-> write_to_Log( $event );
			return false;
        }
		return true;

	}

	/**
	 * Return plugin's name.ab-comments-icon
	 * @return  string    DASH-Sync.
	 */
	public function get_plugin_name() 
	{
		return $this->plugin_name;
	}

	/**
	 * Return Factory object to register other filters and actions
	 * @return  Factory
	 */
	public function get_factory() 
	{
		return $this->factory;
	}

	/**
	 * Retrieve the version number of the plugin.
	 * @return string  The version number of the plugin.
	 */
	public function get_version() 
	{
		return $this->version;
	}

}