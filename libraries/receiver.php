<?php

/**
 * @author Whyte Enterprises
 * @package  DASH-Sync 
 */

/**
 * Receptor class
 */
class Receiver{

    /**
     * Filter and actions register
     * @var string $token to acces information.
     * @access protected 
     */
    protected $token;
    
    /**
     * ID of the API client
     * @var string $client_id to acces information.
     * @access protected 
     */
    protected $client_id;

    /**
     * Client secret key to the API
     * @var string  $client_secret current plugin version 
     * @access protected
     */
    protected $client_secret;

    /**
     * This is the API web address
     * @var string  $api_url API web address
     * @access protected
     */
    protected $api_url;

    

    public function __construct()
    {
        $this->client_id = '6f2a7130cf20443fa4fef363a7c2b5d8';
        $this->api_url = "https://apps.dashplatform.com/dash/jsonapi/api/v1/";
        $this->client_secret = 'ab1bb6e8e3fb7b05d763333f461daa92';        
          
    }

     /**
     * Verify if token is ready for access data. 
     * @access private 
     */

    public function token_ready()
    {
        $response =  $this->request_token();

        if ( !$response['error']) {
            $this->token = $response["response"]->access_token;
            return $response;
        }
        return $response;       
    }
    /**
     * Request authorization token from DASH API
     * @access private 
     */
    private function request_token() 
    {
        $data = array(
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_URL            => $this->api_url .'company/auth/token' ,
            CURLOPT_POST           => TRUE,
            CURLOPT_POSTFIELDS     => http_build_query(
                array(
                    'grant_type'    => "client_credentials",
                    'client_id'     => $this->client_id,
                    'client_secret' => $this->client_secret,
                )
            )
        );

        return  $this->post( $data );        
    }

    /**
     * Request all data needed for synchronization
     * @access public 
     */
    public function request_all_data() 
    {
        $this->events_data_request(1,"c");
    }

    /**
     * Request facilities needed for synchronization
     * @param  integer (optional) $page_size number of records per request, default = 30. 
     * @access public 
     */
    public function falcilities_data_request( $page_size = 30 )
    {
        $asset = 'facilities';
        $parameters = array(
            'page'=> array( 'size' => $page_size ),
        );
        $query = http_build_query( $parameters );

        return $this->data_request( $asset, $query );
    }

    /**
     * Request resources needed for synchronization
     * @param  integer (optional) $page_size number of records per request, default = 30.
     * @param  integer (optional) $page_number number of the page requested to, default = 1. 
     * @access public 
     */
    public function resources_data_request( $page_size = 30, $page_number = 1 )
    {
        $asset = 'resources';
        $parameters = array(
            'include'   => 'facility',
            'page'      => array( 
                'size' => $page_size, 
            ),
        );

        $page_number = trim( $page_number );
        if ( strcmp( $page_number, "" ) != 0 ) {
            $parameters['page']['number'] =  $page_number;
        }

        $query = http_build_query( $parameters );

        return $this->data_request( $asset, $query );
    }

    /**
     * Request event types needed for synchronization
     * @param  integer (optional) $page_size number of records per request, default = 30. 
     * @access public 
     */
    public function event_types_data_request( $page_size = 30 )
    {
        $asset = 'event-types';
        $parameters = array(
            'page'      => array( 'size' => $page_size ),
        );
        $query = http_build_query( $parameters );

        return $this->data_request( $asset, $query );
    }

    /**
     * Request events needed for synchronization
     * @param  integer (optional) $page_size number of records per request.
     * @param  string  (optional) $event_type type of the event requested.  
     * @access public 
     */
    public function events_data_request( $page_size, $page_number, $event_type = "" )
    {
        $date = date("Y-m-d").'T'.date("H:i:s"); // Actual date format: '2020-02-03T00:00:00'
        $asset = 'events';

        //posible includes to add in the event request
        //homeTeam.league.season,visitingTeam.league.season,resource.facility,resourceArea
        $parameters = array(
            'page'      => array( 
                'size' => $page_size,
                'number' => $page_number,
            ),
            'filter'    => array(
                'end__gt' => $date,
            ),
            'include'   => 'eventType',
            'publish'   => 'true',
        );
        
        $event_type = trim($event_type);
        if ( strcmp( $event_type, "" ) != 0 ) {
            $parameters['filter']['event_type__in'] =  $event_type;
        }
       
        $query = http_build_query( $parameters );
       
        return $this->data_request( $asset, $query );
    }

    /**
     * Seasons data request
     * @param  integer (optional) $page_size number of records per request.
     * @param  string  (optional) $event_type type of the event requested.  
     * @access public 
     */
    public function seasons_data_request( $page_size, $page_number )
    {
        $asset = 'seasons';
        $parameters = array(
            'page'      => array( 
                'size' => $page_size,
                'number' => $page_number,
            ),
        );

        $query = http_build_query( $parameters );       
        return $this->data_request( $asset, $query );
    }

     /**
     * Teams data request
     * @param  integer (optional) $page_size number of records per request.
     * @param  string  (optional) $event_type type of the event requested.  
     * @access public 
     */
    public function teams_data_request( $page_size, $page_number )
    {
        $asset = 'teams';
        $parameters = array(
            'page'      => array( 
                'size' => $page_size,
                'number' => $page_number,
            ),
        );

        $query = http_build_query( $parameters );       
        return $this->data_request( $asset, $query );
    }

    /**
     * Leagues data request
     * @param  integer (optional) $page_size number of records per request.
     * @param  string  (optional) $event_type type of the event requested.  
     * @access public 
     */
    public function leagues_data_request( $page_size, $page_number )
    {
        $asset = 'leagues';
        $parameters = array(
            'page'      => array( 
                'size' => $page_size,
                'number' => $page_number,
            ),
        );

        $query = http_build_query( $parameters );       
        return $this->data_request( $asset, $query );
    }

    /**
     * Create curl response from DASH API
     * @access private 
     */

    private function data_request( $asset, $parameters ) 
    {
        $header = array(
                'Authorization: Bearer '.$this->token,
        );      
        
        $url =$this->api_url.$asset.'?'.$parameters;
        
        $data = array(
            CURLOPT_RETURNTRANSFER => TRUE,            
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_URL            => $url,
            CURLOPT_HTTPHEADER     => $header, 
        );
        

        return $this->post( $data );
       
    }

    /**
     * Execute a curl request to DASH API
     * @param array $data request variables (header, method, url, postfieds)
     * @access private
     */
    private function post( $data )
    {
        try{

            $curl = curl_init();
            curl_setopt_array(
                $curl,
                $data
            );    
            $api_data = json_decode( curl_exec( $curl ) );
            $response = array( 'error' => false, 'exception'=>"", "response" => $api_data );

            if ( is_null( $response["response"] ) ){
                $response = array( 'error' => true,  "exception" => "No response in API call! Review network status." ); 
            }

        } catch( Exception $e ){
            $response =  array( 'error' => true, 'exception'  => $e->getMessage() );
        }

        return $response;
    }

}