<?php

/**
 * @author Whyte Enterprises
 * @package  DASH-Sync 
 */

/**
 * DASH_Sync activation class
 */
class Activate_dash_sync{


    /**
     * Activation procedure
     */
    public static function activate()
    {
        /**
         * Create tables
         */
        global $wpdb; 
        require_once plugin_dir_path( __FILE__ ) . '/db_manager.php';
        DBManager::create_tables( $wpdb->prefix );
	
    }

    
}