<?php

/**
 * @author Whyte Enterprises
 * @package  DASH-Sync 
 */

/**
 * Receptor class
 */
class FileManager {


    public function __construct(){
        
        
    }

    public function create_file( $filename, $content ){
       

        if ( !is_file( $filename ) ){           // Some simple example content.
               
            file_put_contents( $filename, $content );     // Save our content to the file.
        
        } else {
            return array( 'error' =>   true, 'data'  => NULL, 'exception' => "Error: File ". $filename. " is no Writable." ); 
        }

    }

    public static function writefile($filename, $content, $operation = 'a' ){

        $file = fopen($filename, $operation );
        fwrite($file, $content) ;
        fclose( $file );    
        
    }

    public static function read_csv_file( $file_name ){

        try{
            ini_set('auto_detect_line_endings',TRUE);
            $rows = array();
            if ( is_file( $file_name ) ) {

                $file = fopen($file_name, "r");
                while ( ($line = fgetcsv($file,$delimiter = ",")) !== FALSE ) {
                    $rows[] = $line;
                }
                fclose( $file );
                return array( 'error' =>   false, 'data'  => $rows );
            }

            return  array( 'error' => true, 'exception'  => 'File: '.$file_name . " not Found!!" );
            

        }catch( Exception $e ){
            return array( 'error' =>   true, 'data'  => NULL, 'exception' => $e->getMessage() );
        }

    }

}
