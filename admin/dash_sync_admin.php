<?php

/**
 * @author Whyte Enterprises
 * @package  DASH-Sync 
 */

/**
 * DASH_Sync activation class
 */
class AdminController{

    protected $logger;
    /**
     * Activation procedure
     */
    public function __construct()
    {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'libraries/logger.php';   
        $this->logger = new Logger(); 
    }   
  
    public function update_notice() {


        $response = $this->logger->get_last_event();             
                
        $data = $response['data']; 
        $last_date =  $data['date'];
        
        if ( $data['event']['error'] ){
            $class = 'notice notice-error';
	        $message = __( 'DASH Sync Notice: ' . $data['event']['exception']. " Last attempt: ". $last_date  );
        } else {
            $class = 'notice notice-success is-dismissible';
	        $message = __( 'DASH Sync Notice: ' . $data['event']['exception']. " Last attempt: ". $last_date  );
        }
   
        
	    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
    }
    
}
?>