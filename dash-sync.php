<?php
/**
*	Plugin Name: DASH Event synchronizer
*	Plugin URI: https://joel.mydev.ca/
*	Description: Syncronize DASH events into Modern Events Calendar. 
*	Author: Whyte Enterprises
*	Version: 1.0.0
*   Text Domain: DASH-Sync
**/

/**
 * @author Whyte Enterprises
 * @package  DASH-Sync 
 */

 /**
  * No Access through direct methods
  */
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Activate plugin main function
 */
function activate_dash_sync() 
{
	require_once plugin_dir_path( __FILE__ ) . 'libraries/dash_sync_activate.php';
    Activate_dash_sync::activate();    
    exec_dash_sync();    
}

/**
 * Execute DASH synchronization events plugin 
 */
function exec_dash_sync() 
{
    require_once plugin_dir_path( __FILE__ ) . 'libraries/main.php';    
	$dash_sync = new DASHSync();
    $dash_sync->run();
}

/**
 * Crate cron job. 
 */
function create_cron_job() 
{
    require_once plugin_dir_path( __FILE__ ) . 'libraries/synchronize.php';
    $MyCron = new MyCron();

    require_once plugin_dir_path( __FILE__ ) . 'admin/dash_sync_admin.php';
    $admin_controller = new AdminController( );
    add_action("admin_notices",array($admin_controller,'update_notice'));
    //
}

/**
 * Deactivate plugin main function
 */

function deactivate_dash_sync() 
{
	require_once plugin_dir_path( __FILE__ ) . 'libraries/dash_sync_deactivate.php';
    Deactivate_dash_sync::deactivate();
}


/**
* Register activation, deactivation, and uninstall hooks
*/
function load_hooks()
{
    register_activation_hook( __FILE__ , 'activate_dash_sync' );
    register_deactivation_hook( __FILE__ , 'deactivate_dash_sync');
}



define( "DASH_SYNC_VERSION", "1.0.0" );
define( "DASH_SYNC_DB_PREFIX", "das_" );
define( "DASH_SYNC_NULL_FLAG", -999 );

//Please use 24h and xx:xx:xx formats e.g. 22:00:00
define( "START_TIME", "22:00:00" ); //hour to start sinchronization.

//Please use a float number e.g. 3.0
define( "RANGE_OF_TIME", "6.0" ); //Number of hours to try sinchronization.



load_hooks();
create_cron_job();
