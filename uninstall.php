<?php

/**
 * @author Whyte Enterprises
 * @package  DASH-Sync 
 */

if ( ! defined('WP_UNINSTALL_PLUGIN') ) {
    die;
}

 /** 
  * Code for uninstalling
  */ 

require_once plugin_dir_path( __FILE__ ) . 'libraries/db_manager.php';

$facility = $prefix . DASH_SYNC_DB_PREFIX . "facility"; 
$resource = $prefix . DASH_SYNC_DB_PREFIX . "resource";
$eventType = $prefix . DASH_SYNC_DB_PREFIX . "eventType";
$event = $prefix . DASH_SYNC_DB_PREFIX . "event";
$season = $prefix . DASH_SYNC_DB_PREFIX . "season";
$league = $prefix . DASH_SYNC_DB_PREFIX . "league";
$team = $prefix . DASH_SYNC_DB_PREFIX . "team";
$eventTypeCategory = $prefix . DASH_SYNC_DB_PREFIX . "eventType_category";

DBManager::drop_table( $facility );
DBManager::drop_table( $resource );
DBManager::drop_table( $eventType );
DBManager::drop_table( $season );
DBManager::drop_table( $league );
DBManager::drop_table( $team );
DBManager::drop_table( $eventTypeCategory );